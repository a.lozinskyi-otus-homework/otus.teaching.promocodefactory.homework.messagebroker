﻿using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Requests.Promocode
{
    public class CreatePromocodeRequest
    {
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }

        public Guid PreferenceId { get; set; }
    }
}
