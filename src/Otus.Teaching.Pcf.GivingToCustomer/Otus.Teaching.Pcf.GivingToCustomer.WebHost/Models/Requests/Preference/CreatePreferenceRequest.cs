﻿namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Requests.Preference
{
    public class CreatePreferenceRequest
    {
        public string Name { get; set; }
    }
}
