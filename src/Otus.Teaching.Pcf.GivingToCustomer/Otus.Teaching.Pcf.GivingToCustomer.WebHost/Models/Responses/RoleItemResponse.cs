﻿using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Responses
{
    public class RoleItemResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
    }
}