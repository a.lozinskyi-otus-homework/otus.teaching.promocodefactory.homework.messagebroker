﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Responses
{
    public class CustomerResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<PreferenceResponse> Preferences { get; set; } = new List<PreferenceResponse>();
        public List<PromoCodeShortResponse> PromoCodes { get; set; } = new List<PromoCodeShortResponse>();
    }
}