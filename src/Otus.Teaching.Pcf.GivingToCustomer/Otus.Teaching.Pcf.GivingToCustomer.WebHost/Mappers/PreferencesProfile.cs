﻿using AutoMapper;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Requests.Preference;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Responses;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public class PreferencesProfile : Profile
    {
        public PreferencesProfile()
        {
            CreateMap<CreatePreferenceRequest, Preference>();
            CreateMap<UpdatePreferenceRequest, Preference>()
                .ReverseMap();
            CreateMap<Preference, PreferenceResponse>();
        }
    }
}
