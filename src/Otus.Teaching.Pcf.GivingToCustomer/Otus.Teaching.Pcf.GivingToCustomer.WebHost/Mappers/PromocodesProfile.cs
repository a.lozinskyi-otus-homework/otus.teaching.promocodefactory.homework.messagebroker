﻿using System;
using AutoMapper;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Requests.Promocode;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Responses;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public class PromocodesProfile : Profile
    {
        public PromocodesProfile()
        {
            CreateMap<PromoCode, PromoCodeShortResponse>()
                .ForMember(d => d.BeginDate, opts => opts.MapFrom(src => src.BeginDate.ToString("yyyy-MM-dd")))
                .ForMember(d => d.EndDate, opts => opts.MapFrom(src => src.EndDate.ToString("yyyy-MM-dd")));

            CreateMap<Customer, PromoCodeCustomer>()
                .ForMember(d => d.CustomerId, opts => opts.MapFrom(src => src.Id));

            CreateMap<Preference, PromoCode>();
            CreateMap<CreatePromocodeRequest, PromoCode>()
                .ForMember(d => d.BeginDate, opts => opts.MapFrom(src => DateTime.Parse(src.BeginDate)))
                .ForMember(d => d.EndDate, opts => opts.MapFrom(src => DateTime.Parse(src.EndDate)))
                .ReverseMap()
                .ForMember(d => d.BeginDate, opts => opts.MapFrom(src => src.BeginDate.ToString("yyyy-MM-dd")))
                .ForMember(d => d.EndDate, opts => opts.MapFrom(src => src.EndDate.ToString("yyyy-MM-dd")));

            CreateMap<UpdatePromocodeRequest, PromoCode>()
                .IncludeBase<CreatePromocodeRequest, PromoCode>()
                .ReverseMap();
        }
    }
}
