﻿using AutoMapper;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Requests.Customer;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Responses;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public class CustomersProfile : Profile
    {
        public CustomersProfile()
        {
            CreateMap<Customer, CustomerShortResponse>();
            CreateMap<CustomerPreference, PreferenceResponse>()
                .ForMember(d => d.Id, opts => opts.MapFrom(src => src.PreferenceId))
                .ForMember(d => d.Name, opts => opts.MapFrom(src => src.Preference.Name));
            CreateMap<PromoCodeCustomer, PromoCodeShortResponse>()
                .ForMember(d => d.Id, opts => opts.MapFrom(src => src.PromoCode.Id))
                .ForMember(d => d.BeginDate, opts => opts.MapFrom(src => src.PromoCode.BeginDate.ToString("yyyy-MM-dd")))
                .ForMember(d => d.EndDate, opts => opts.MapFrom(src => src.PromoCode.EndDate.ToString("yyyy-MM-dd")))
                .ForMember(d => d.Code, opts => opts.MapFrom(src => src.PromoCode.Code))
                .ForMember(d => d.PartnerId, opts => opts.MapFrom(src => src.PromoCode.PartnerId))
                .ForMember(d => d.ServiceInfo, opts => opts.MapFrom(src => src.PromoCode.ServiceInfo));
            CreateMap<Customer, CustomerResponse>();
            CreateMap<Preference, CustomerPreference>()
                .ForMember(d => d.PreferenceId, opts => opts.MapFrom(src => src.Id));
            CreateMap<CreateCustomerRequest, Customer>()
                .ReverseMap();
            CreateMap<UpdateCustomerRequest, Customer>()
                .IncludeBase<CreateCustomerRequest, Customer>()
                .ReverseMap();

        }
    }
}
