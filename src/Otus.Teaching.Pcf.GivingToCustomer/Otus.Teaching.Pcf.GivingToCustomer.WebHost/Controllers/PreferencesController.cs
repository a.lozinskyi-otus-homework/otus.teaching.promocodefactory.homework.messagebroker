﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Requests.Preference;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Responses;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    public class PreferencesController : Controller
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IMapper _mapper;

        public PreferencesController(IRepository<Preference> preferencesRepository, IMapper mapper)
        {
            _preferencesRepository = preferencesRepository;
            _mapper = mapper;
        }

        public ActionResult Create()
        {
            return View("Create");
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreatePreferenceRequest request)
        {
            var preference = _mapper.Map<Preference>(request);
            await _preferencesRepository.AddAsync(preference);
            return RedirectToAction(nameof(Index));
        }

        public async Task<ActionResult<List<PreferenceResponse>>> Index()
        {
            var preferences = await _preferencesRepository.GetAllAsync();
            return View(preferences.Select(_mapper.Map<PreferenceResponse>));
        }

        public async Task<ActionResult<PreferenceResponse>> Details(Guid id)
        {
            var preference = await _preferencesRepository.GetByIdAsync(id);
            return View("Details", _mapper.Map<PreferenceResponse>(preference));
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            var preference = await _preferencesRepository.GetByIdAsync(id);
            return View("Edit", _mapper.Map<UpdatePreferenceRequest>(preference));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Guid id, UpdatePreferenceRequest request)
        {
            var preference = await _preferencesRepository.GetByIdAsync(id);
            _mapper.Map(request, preference);
            await _preferencesRepository.UpdateAsync(preference);
            return RedirectToAction(nameof(Index));
        }

        public async Task<ActionResult> Delete(Guid id)
        {
            var preference = await _preferencesRepository.GetByIdAsync(id);
            await _preferencesRepository.DeleteAsync(preference);
            return RedirectToAction(nameof(Index));
        }
    }
}