﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Requests.Promocode;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Responses;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    public class PromocodesController : Controller
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly IMapper _mapper;

        public PromocodesController(IRepository<PromoCode> promoCodesRepository, 
            IRepository<Preference> preferencesRepository, IRepository<Customer> customersRepository, IMapper mapper)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить промокод
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult<PromoCodeShortResponse>> Details(Guid id)
        {
            var promocode = await _promoCodesRepository.GetByIdAsync(id);
            return View("Details", _mapper.Map<PromoCodeShortResponse>(promocode));
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult<List<PromoCodeShortResponse>>> Index()
        {
            var promocodes = await _promoCodesRepository.GetAllAsync();
            return View("Index", promocodes.Select(_mapper.Map<PromoCodeShortResponse>));
        }

        public ActionResult Create()
        {
            return View("Create");
        }
        
        /// <summary>
        /// Создать промокод
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create(CreatePromocodeRequest request)
        {
            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(request.PreferenceId);
            if (preference == null)
            {
                return BadRequest();
            }

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>x.Preference.Id == preference.Id));
            var promocode = _mapper.Map<PromoCode>(request);
            promocode.Customers = _mapper.Map<List<PromoCodeCustomer>>(customers);
            await _promoCodesRepository.AddAsync(promocode);
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            var promocode = await _promoCodesRepository.GetByIdAsync(id);
            return View("Edit", _mapper.Map<UpdatePromocodeRequest>(promocode));
        }

        /// <summary>
        /// Обновить промокод
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Edit(Guid id, UpdatePromocodeRequest request)
        {
            var promocode = await _promoCodesRepository.GetByIdAsync(id);
            if (promocode == null)
            {
                return NotFound();
            }
            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(request.PreferenceId);
            if (preference == null)
            {
                return BadRequest();
            }

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x => x.Preference.Id == preference.Id));
            _mapper.Map(request, promocode);
            promocode.Customers = _mapper.Map<List<PromoCodeCustomer>>(customers);
            await _promoCodesRepository.UpdateAsync(promocode);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Удалить промокод
        /// </summary>
        public async Task<ActionResult> Delete(Guid id)
        {
            var promocode = await _promoCodesRepository.GetByIdAsync(id);
            await _promoCodesRepository.DeleteAsync(promocode);
            return RedirectToAction("Index");
        }
    }
}