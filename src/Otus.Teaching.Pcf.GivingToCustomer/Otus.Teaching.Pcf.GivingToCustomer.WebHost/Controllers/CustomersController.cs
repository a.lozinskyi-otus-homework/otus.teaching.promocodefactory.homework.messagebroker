﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Requests.Customer;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models.Responses;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    public class CustomersController : Controller
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IMapper _mapper;

        public CustomersController(IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult<List<CustomerShortResponse>>> Index()
        {
            var customers =  await _customerRepository.GetAllAsync();
            return View("Index", customers.Select(_mapper.Map<CustomerShortResponse>));
        }
        
        /// <summary>
        /// Получить клиента по id
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <returns></returns>
        public async Task<ActionResult<CustomerResponse>> Details(Guid id)
        {
            var customer =  await _customerRepository.GetByIdAsync(id);
            return View("Details", _mapper.Map<CustomerResponse>(customer));
        }

        public ActionResult Create()
        {
            return View("Create");
        }

        /// <summary>
        /// Создать нового клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> Create(CreateCustomerRequest request)
        {
            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            var customer = _mapper.Map<Customer>(request);
            customer.Preferences = preferences.Select(_mapper.Map<CustomerPreference>).ToList();
            await _customerRepository.AddAsync(customer);

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            return View("Edit", _mapper.Map<UpdateCustomerRequest>(customer));
        }

        /// <summary>
        /// Обновить клиента
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <param name="request">Данные запроса></param>
        [HttpPost]
        public async Task<IActionResult> Edit(Guid id, UpdateCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }
            
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            _mapper.Map(request, customer);
            customer.Preferences = preferences.Select(_mapper.Map<CustomerPreference>).ToList();
            await _customerRepository.UpdateAsync(customer);

            return RedirectToAction("Index");
        }
        
        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        public async Task<IActionResult> Delete(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            await _customerRepository.DeleteAsync(customer);
            return RedirectToAction("Index");
        }
    }
}