using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Application;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.WebHost.RabbitMq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Otus.Teaching.Pcf.Administration.WebHost.HostedServices
{
    public class ListeningToRabbitService : BackgroundService
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly RabbitMqOptions _rabbitMqOptions;
        
        public ListeningToRabbitService(IServiceScopeFactory scopeFactory, IOptions<RabbitMqOptions> rabbitMqOptions)
        {
            _scopeFactory = scopeFactory;
            _rabbitMqOptions = rabbitMqOptions.Value;
        }
        
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using var connection = _rabbitMqOptions.GetConnection();
            using var channel = connection.CreateModel();
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += async (model, ea) =>
            {
                var body = ea.Body;
                var id = Encoding.UTF8.GetString(body.ToArray());
                using var scope = _scopeFactory.CreateScope();
                var employeeRepository = scope.ServiceProvider.GetService<IRepository<Employee>>();
                var employeeService = new EmployeeService(employeeRepository);
                await employeeService.UpdateGivenPromocodes(Guid.Parse(id));
            };

            while (!stoppingToken.IsCancellationRequested)
            {
                channel.BasicConsume(queue: _rabbitMqOptions.QueueName, autoAck: true, consumer: consumer);
                await Task.Delay(5000, stoppingToken);
            }
        }
    }
}