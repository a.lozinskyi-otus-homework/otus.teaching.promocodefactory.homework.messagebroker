using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.Administration.WebHost.RabbitMq
{
    public class RabbitMqOptions
    {
        public string Host { get; set; }
        
        public string UserName { get; set; }
        
        public string Password { get; set; }
        
        public string VirtualHost { get; set; }
        
        public string QueueName { get; set; }
 
        public int Port { get; set; }
        
        public IConnection GetConnection ()
        {
            var factory = new ConnectionFactory
            {
                UserName = UserName,
                Password = Password,
                VirtualHost = VirtualHost,
                HostName = Host,
                Port = Port
            };

            return factory.CreateConnection();
        }    
    }
}